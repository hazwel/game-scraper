import scrapy
from scrapy_splash import SplashRequest 


class GamespiderSpider(scrapy.Spider):
    name = "gamespider"
    allowed_domains = ["www.gamesdatabase.org"]
    start_urls = ["https://www.gamesdatabase.org/list"]
    
    # Replacing default request with Splashrequest
    def start_requests(self): 
        for url in self.start_urls: 
            yield SplashRequest(url, self.parse, 
                #endpoint='render.html', 
                args={'wait': 0.5}, 
                )
    
    def parse(self, response):
        
        games_list = response.css("tr[align='left']")
        
        # Get attribute
        # response.css("tr").attrib["align"]
        
        # [0] = row names
        # games_list[0].css('th').extract()
        # Game, System, Publisher, Developer, Category, Year
        
        cols = {
            1 : "Game",
            2 : "System",
            4 : "Publisher",
            6 : "Developer",
            7 : "Category",
            8 : "Year"
        }
        
        for row in games_list:
            
            print("\n")
            game = row.css("td a")
            
            for i in range(len(game)):
                
                if i == 0: # Row names
                    continue
                
                text = game[i].css('a::text').get()
                
                if text == None:
                    text = "-"
                
                try:
                    yield { cols[i] : text }
                
                except KeyError:
                    continue


